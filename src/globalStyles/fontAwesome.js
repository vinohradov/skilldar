import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faCheckSquare,
  faCoffee,
  faClock,
  faMapMarkerAlt,
  faSearch,
} from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';

library.add(fab, faCheckSquare, faClock, faMapMarkerAlt, faCoffee, faSearch);
